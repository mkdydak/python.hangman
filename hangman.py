import random


HANGMAN = ['''
  +---+
      |
      |
      |
     ===''', '''
  +---+
  O   |
      |
      |
     ===''', '''
  +---+
  O   |
  |   |
      |
     ===''', '''
  +---+
  O   |
 /|   |
      |
     ===''', '''
  +---+
  O   |
 /|\  |
      |
     ===''', '''
  +---+
  O   |
 /|\  |
 /    |
     ===''', '''
  +---+
  O   |
 /|\  |
 / \  |
     ===''']


words = ["question", "answer", "science"]

chosen_word = random.choice(words)
guessed_word = ['_'] * len(chosen_word)


max_tries = len(HANGMAN) - 1
tries = 0


used_letters = []


print("Welcome to 'Hangman' game!")

while tries < max_tries and '_' in guessed_word:
    print(HANGMAN[tries])
    print(f"Word: {' '.join(guessed_word)}")
    print(f"Used letters: {', '.join(used_letters)}")
    
    guess = input("Enter a letter: ").lower()
    

    if guess in used_letters:
        print("You have already guessed that letter.")
        continue
    else:
        used_letters.append(guess)
    

    if guess in chosen_word:
        for i in range(len(chosen_word)):
            if chosen_word[i] == guess:
                guessed_word[i] = guess
    else:
        print(f"The letter '{guess}' is not in the word.")
        tries += 1


if '_' not in guessed_word:
    print("Congratulations! You've saved the hangman and won the game!")
else:
    print(HANGMAN[tries])
    print("The hangman has been hanged. You lost!")
    print(f"The correct word was: '{chosen_word}'.")


